function get(url, metodo = "GET") {
  // Return a new promise.
  return new Promise(function (resolve, reject) {
    // Do the usual XHR stuff
    var req = createXHR();
    req.open(metodo, url);

    req.onload = function () {
      // This is called even on 404 etc
      // so check the status
      if (req.status == 200) {
        // Resolve the promise with the response text
        resolve(req.response);
      } else {
        // Otherwise reject with the status text
        // which will hopefully be a meaningful error
        reject(Error(req.statusText));
      }
    };

    // Handle network errors
    req.onerror = function () {
      reject(Error("La api no esta funcionando"));
    };

    // Make the request
    req.send();
  });
}

function createXHR() {
  var request = false;
  try {
    request = new ActiveXObject("Msxml2.XMLHTTP");
  } catch (err2) {
    try {
      request = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (err3) {
      try {
        request = new XMLHttpRequest();
      } catch (err1) {
        request = false;
      }
    }
  }
  return request;
}

/**
 * Agrega una cabecera a una tabla
 * @param {*} data Nombre de la cabecera
 */
function agregarCabecera(...data) {
  var div = document.createElement("div");
  div.setAttribute("class", "content-table");
  div.setAttribute("id", "content");
  var table = document.createElement("table");
  table.setAttribute("class", "content-table");
  div.appendChild(table);
  var thead = document.createElement("thead");
  table.appendChild(thead);
  var cabecera = document.createElement("tr");
  thead.appendChild(cabecera);
  for (var txt of data) {
    var th = document.createElement("th");
    var h3 = document.createElement("h3");

    h3.appendChild(document.createTextNode(txt));
    th.appendChild(h3);
    cabecera.appendChild(th);
  }

  return table;
}

/**
 * Añade un tr con datos en el cuerpo de la tabla
 * @param  {...any} data uno o varios datos para añadir
 */
function putTrOnTheBody(...data) {
  var tr = document.createElement("tr");

  for (var text of data) {
    
    var th = document.createElement("th");
    th.appendChild(document.createTextNode(text));
    tr.appendChild(th);
  }

  var thbtnEditar = document.createElement("th");
  var cbtnEditar = document.createElement("div");
  cbtnEditar.setAttribute("class", "container");
  cbtnEditar.innerText = "Edit";
  cbtnEditar.addEventListener("click", function () {
    editPais(data[0]);
  
  });
  
  thbtnEditar.appendChild(cbtnEditar);
  tr.appendChild(thbtnEditar);


  var thbtnEliminar = document.createElement("th");
  btnDelete = document.createElement("div");
  btnDelete.setAttribute("class", "container");
  btnDelete.innerText = "Delete";
  thbtnEliminar.addEventListener("click", function () {
    deletePais(data[0]);
  
  });
  thbtnEliminar.appendChild(btnDelete);
  tr.appendChild(thbtnEliminar);

  return tr;
}


/**
 * Limpia el contenido de la cabecera
 */
function clearTable() {
  var table = document.getElementById("tableInfo");
  table.innerHTML = "";
}

function editPais(id) {
  console.log("editar id: " + id);
}
function deletePais(id) {
  get(API_URL + id, "delete")
    .then(function (response) {
      var res = JSON.parse(response);
      if (res.affectedRows > 0 && res.serverStatus === 2) {
        alert("Eliminado el pais con el id " + id);
      } else {
        alert("No se ha podido eliminar el pais con el id " + id);
      }
    })
    .catch(function (error) {
      console.error(error);
    });
}
