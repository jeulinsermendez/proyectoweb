var req;

const API_URL = "http://localhost:3000/api/cp_pais/";

get(API_URL, "GET")
  .then(function (response) {
    presentaLista(JSON.parse(response));
  })
  .catch(function (error) {
    console.error(error);
  });

get(API_URL + "count")
  .then(function (response) {
    obtenDatos(JSON.parse(response));
    agregarCabecera();
  })
  .catch(function (error) {
    console.error(error);
  });

function presentaLista(respuesta) {
  clearTable();
  let contentTable = document.getElementById("tableInfo");

  var table = agregarCabecera(
    "id",
    "ISO",
    "Nombre",
    "Nombre Español",
    "Num. Codigo",
    "Editar",
    "Borrar"
  );
  var tbody = document.createElement("tbody");
  table.appendChild(tbody);
  contentTable.appendChild(table);
  respuesta.forEach((row) => {
    var rowTable = putTrOnTheBody(
      row.country_id,
      row.iso2,
      row.short_name,
      row.spanish_name,
      row.numcode
    );
    tbody.appendChild(rowTable);
    //     tr = document.createElement("tr");
    //  var container = document.createElement("div");
    //      container.setAttribute("id",row.country_id);
    //      container.setAttribute("class","container");
    //     td = document.createElement("td");
    //     td.appendChild(document.createTextNode(row.country_id));
    //     container.appendChild(td);
    //     tr.appendChild(td);
    //     td = document.createElement("td");
    //     td.appendChild(document.createTextNode(row.iso2));
    //     tr.appendChild(td);
    //     td = document.createElement("td");
    //     td.appendChild(document.createTextNode(row.short_name));
    //     tr.appendChild(td);
    //     td = document.createElement("td");
    //     td.appendChild(document.createTextNode(row.spanish_name));
    //     tr.appendChild(td);
    //     td = document.createElement("td");
    //     td.appendChild(document.createTextNode(row.numcode));
    //     tr.appendChild(td);
    //     td = document.createElement("td");

    //     var container = document.createElement("div");
    //     container.setAttribute("class","container");
    //     container.innerText = "Edit";
    //     container.addEventListener("click",function (){
    //         editPais(row.country_id);
    //     });
    //     td.appendChild(container);
    //     tr.appendChild(td);

    //     var container2 = document.createElement("div");
    //     container2.setAttribute("class","container");

    //     container2.innerText = "Delete";
    //     container2.addEventListener("click",function (){
    //         deletePais(row.country_id);
    //     });
    //     td.appendChild(container2);
    //     tr.appendChild(td);
    //     btnDelete = document.createElement("button");
    //     btnDelete.innerText = "Delete";
    //     td = document.createElement("td");
    //     td.appendChild(container2);
    //     tr.appendChild(td);
    //     tbody.appendChild(tr);
  });
}




//{"fieldCount":0,"affectedRows":1,"insertId":0,"serverStatus":2,"warningCount":0,"message":"","protocol41":true,"changedRows":0}

// function processReqChange() {
//     if (req.readyState == 4 && req.status == 200) {
//         var response = JSON.parse(req.responseText);
//         console.log(req);
//         presentaLista(response);
//         loadXMLDoc(API_URL + "count", "", processReqChange_datos);

//     }
// }
// function processReqChange_datos() {
//     if (req.readyState == 4 && req.status == 200) {
//         var response = JSON.parse(req.responseText);
//         console.log(req);
//         obtenDatos(response);
//     }
// }

function obtenDatos(respuesta) {
  var registros = respuesta[0].no_of_rows;
  console.log(registros);
}

//----------------------------------------
// global variable to hold request object

// function loadXMLDoc(url, params, tratamiento) {
//     let req = ajustaReq();
//     console.log(url);
//     if (req) {
//         req.onreadystatechange = tratamiento;
//         req.open("GET", url + '?' + params, true);
//         req.send(null);
//         console.log("Peticion hecha");
//         return true;
//     }
//     return false;
// }
// function ajustaReq() {
//     if (window.XMLHttpRequest) {
//         try {
//             req = new XMLHttpRequest();
//         } catch (e) {
//             req = false;
//         }
//     } else if (window.ActiveXObject) {
//         try {
//             req = new ActiveXObject("Msxml2.XMLHTTP");
//         } catch (e) {
//             try {
//                 req = new ActiveXObject("Microsoft.XMLHTTP");
//             } catch (e) {
//                 req = false;
//             }
//         }
//     }
//     return req;
// }

// loadXMLDoc(API_URL, "", processReqChange);
